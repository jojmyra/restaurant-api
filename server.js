const http = require('http');
const app = require('./app');
const config = require("config");

const port = config.get("PORT") || 3000;

const server = http.createServer(app);

server.listen(port, () => console.log(`server running on port ${port}`));