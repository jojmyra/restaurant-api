// Order.model.js
const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

const orderSchema = mongoose.Schema(
  {
    table: {
      type: String,
      required: "table is required"
    },
    customerNumber: {
      type: Number,
      required: "customerNumber is required"
    },
    menu: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "FoodMenuItem"
      }
    ],
    orderStatus: { type: String, default: "อยู่ในกระบวนการ" }
  },
  {
    timestamps: true
  }
);

orderSchema.plugin(mongoosePaginate);

var Order = mongoose.model("Order", orderSchema);
module.exports = Order;
