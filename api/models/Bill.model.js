// Bill.model.js
const mongoose = require('mongoose')
const mongoosePaginate = require("mongoose-paginate-v2");

const CounterSchema = mongoose.Schema({
  _id: {type: String, required: true},
  seq: { type: Number, default: 0 }
});
var counter = mongoose.model('counter', CounterSchema);

const billSchema = mongoose.Schema({
  billId: { type: Number },
  totalMenu: { type: Number },
  totalPrice: { type: Number },
  recieved: { type: Number },
  change: { type: Number },
  time: { type: String },
  date: { type: Date, default: Date.now() },
  order: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Order",
    default: null
  },
  cashier: {
    type: String,
    required: "cashier is required"
  },
  cashierRef: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    default: null
  },
}, {
  timestamps: true
})

billSchema.pre('save', function(next) {
  var doc = this;
  counter.findByIdAndUpdate({_id: 'entityId'}, {$inc: { seq: 1} }, { upsert: true, new: true }, function(error, counter)   {
      if(error)
          return next(error);
      doc.billId = counter.seq;
      next();
  });
});

billSchema.plugin(mongoosePaginate);

var Bill = mongoose.model('Bill', billSchema)
module.exports = Bill