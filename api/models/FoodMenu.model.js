// FoodMenu.model.js
const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

const foodMenuSchema = mongoose.Schema({
  foodId: { type: String },
  foodName: {
    type: String,
    trim: true,
    unique: true,
    index: true,
    required: "foodName is required"
  },
  foodType: { type: String, required: "foodType is required" },
  price: { type: Number, required: "price is required" },
  describe: { type: String },
  calroies: { type: Number, required: "calories is required" },
  foodImage: { type: String }
}, {
  timestamps: true
});

foodMenuSchema.plugin(mongoosePaginate);

var foodMenu = mongoose.model("FoodMenu", foodMenuSchema);
module.exports = foodMenu;
