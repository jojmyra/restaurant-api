// CashierLog.model.js
const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

const cashierLogSchema = mongoose.Schema(
  {
    cashier: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    startMoney: {
      type: Number,
      default: 0
    },
    stopMoney: {
      type: Number,
      default: 0
    },
    orderValue: {
      type: Number,
      default: 0
    },
    dateTime: { type: Date, default: Date.now() }, 
  },
  {
    timestamps: true
  }
);

cashierLogSchema.plugin(mongoosePaginate);

var CashierLog = mongoose.model("cashierLog", cashierLogSchema);
module.exports = CashierLog;
