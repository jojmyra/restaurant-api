// Cashier.model.js
const mongoose = require("mongoose");

const cashierSchema = mongoose.Schema(
  {
    _id: String,
    cashier: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      default: null
    },
    initialMoney: {
      type: Number,
      default: 0
    },
    orderValue: {
      type: Number,
      default: 0
    }
  },
  {
    timestamps: true
  }
);

var Cashier = mongoose.model("cashier", cashierSchema);
module.exports = Cashier;
