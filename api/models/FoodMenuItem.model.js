// FoodMenuItem.model.js
const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const FoodMenu = require("./FoodMenu.model");

const FoodMenuItemSchema = mongoose.Schema(
  {
    quantity: { type: Number, required: "quantity is required" },
    sumPrice: { type: Number, required: "sumPrice is required" },
    status: { type: String, default: "ได้รับรายการ" },
    time: { type: Date, default: Date.now() },
    tableId: { type: String, required: "tableId is required" },
    foodName: { type: String, required: "foodName is required" },
    foodType: { type: String, required: "foodType is required" },
    price: { type: Number, required: "price is required" },
    describe: { type: String },
    calroies: { type: Number, required: "calories is required" },
    user: { type: String, required: "user is required" },
    kitchen: { type: String },
    serve: { type: String },
    holdOrder: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      default: null
    },
    foodDetail: { type: String, default: "-" },
    cancelOrder: {
      by: String,
      number: Number,
      user: String
    }
  },
  {
    timestamps: true
  }
);

FoodMenuItemSchema.plugin(mongoosePaginate);

var FoodMenuItem = mongoose.model("FoodMenuItem", FoodMenuItemSchema);
module.exports = FoodMenuItem;
