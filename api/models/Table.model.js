// Table.model.js
const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

const tableSchema = mongoose.Schema({
  tableId: {
    type: String,
    unique: true,
    index: true,
    require: "tableId is required"
  },
  number: { type: Number, default: 0 },
  numberSeat: { type: String, require: "numberSeat is required" },
  order: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Order",
    default: null
  },
  status: { type: String, default: "ว่าง" },
  acessCode: { type: String, default: null }
}, {
  timestamps: true
});

tableSchema.plugin(mongoosePaginate);

var Table = mongoose.model("Table", tableSchema);
module.exports = Table;
