// LineClient.model.js
const mongoose = require('mongoose')

const lineClientSchema = mongoose.Schema({
  userId: { type: String },
  tableId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Table",
    default: null
  }
}, {
  timestamps: true
})

var LineClient = mongoose.model('LineClient', lineClientSchema)
module.exports = LineClient