// Order.model.js
const mongoose = require("mongoose");

const systemSchema = mongoose.Schema(
  {
    _id: String,
    shopName: {
      type: String,
      trim: true,
      lowercase: true,
      unique: true,
      index: true,
      required: "ShopName is required"
    },
    emailContact: {
      type: String,
      required: "Email is required"
    },
    telephoneContact: {
      type: String,
      required: "Telephone is required"
    },
    address: {
      type: String,
      required: "Addres is required"
    }
  },
  {
    timestamps: true
  }
);

var System = mongoose.model("System", systemSchema);
module.exports = System;
