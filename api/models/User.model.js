// User.model.js
const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

const userSchema = mongoose.Schema({
  username: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    index: true,
    required: "username is required"
  },
  password: {
    type: String,
    trim: true,
    minlength: [6, "กรุณากรอกรหัสผ่านอย่างน้อย 6 ตัว"],
    required: "password is required"
  },
  prefixName: { type: String, trim: true, required: "prefixName is required" },
  firstName: { type: String, trim: true, required: "firstName is required" },
  lastName: { type: String, trim: true, required: "lastName is required" },
  telephone: {
    type: String,
    trim: true,
    unique: true,
    index: true,
    minlength: [10, "กรุณากรอกเบอร์โทรศัพท์ครบ 10 ตัว"],
    maxlength: [10, "กรุณากรอกเบอร์โทรศัพท์ครบ 10 ตัว"],
    required: "telephone is required"
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    index: true,
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      "กรุณากรอกอีเมล์ให้ถูกต้อง"
    ],
    required: "email is required"
  },
  role: { type: String, trim: true, required: "role is required" },
});

userSchema.plugin(mongoosePaginate);

var User = mongoose.model("User", userSchema);
module.exports = User;
