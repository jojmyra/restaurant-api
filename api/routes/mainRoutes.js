const express = require('express')
const router = express.Router()

const routesUser = require('./routesUser')
const routesTable = require('./routeTable')
const routesFoodMenu = require('./routesFoodMenu')
const routesOrder = require('./routeOrder')
const routesCashier = require('./routesCashier')
const routesSystem = require('./routesSystem')
const routesLine = require('./routesLine')

const checkAuth = require('../middleware/check-auth')

router.use('/user', routesUser)
router.use('/cashier', checkAuth, routesCashier)
router.use('/table', checkAuth, routesTable)
router.use('/menu', checkAuth, routesFoodMenu)
router.use('/order', checkAuth, routesOrder)
router.use('/setting', checkAuth, routesSystem)
router.use('/line', routesLine)

module.exports = router