const express = require('express')
const router = express.Router()

const controller = require('../controllers/controllerTable')

router.get(`/`, controller.getAll)
router.post(`/`, controller.add)
router.put(`/:id`, controller.edit)
router.delete(`/:id`, controller.delete)

module.exports = router