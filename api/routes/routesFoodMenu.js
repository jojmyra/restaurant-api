const express = require('express')
const router = express.Router()

const multer = require(`multer`)

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads');
    },
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fieldSize: 5 * 1024 * 1024
    },
    fileFilter: fileFilter
})

const controller = require('../controllers/controllerFoodMenu')

router.get(`/`, controller.getAll)
router.post(`/`, upload.single('foodImage'), controller.add)
router.put(`/:id`, controller.edit)
router.delete(`/:id`, controller.delete)

module.exports = router