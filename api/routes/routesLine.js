const express = require('express')
const router = express.Router()

const controller = require('../controllers/controllerLine')


router.use(`/`, controller.defaultFallBack)

module.exports = router