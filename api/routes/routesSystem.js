const express = require('express')
const router = express.Router()

const controller = require('../controllers/controllerSystem')

router.get(`/`, controller.get)
router.post(`/`, controller.config)

module.exports = router