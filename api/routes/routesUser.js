const express = require('express')
const router = express.Router()

const controller = require('../controllers/controllerUser')

const checkAuth = require('../middleware/check-auth')

router.get(`/`, checkAuth, controller.getAll)
router.get(`/cashier`, checkAuth, controller.getCashier)
router.post(`/login`, controller.login)
router.post(`/refresh-token`, controller.refreshToken)
router.post(`/changepassword`, checkAuth, controller.changePassword)
router.post(`/`, checkAuth, controller.add)
router.put(`/:id`, checkAuth, controller.edit)
router.delete(`/:id`, checkAuth, controller.delete)

module.exports = router