const express = require("express");
const router = express.Router();

const controller = require("../controllers/controllerCashier");

router.get(`/`, controller.getCashier);
router.get(`/log`, controller.getAllCashierLog);
router.post(`/`, controller.attendCashier);
router.put(`/`, controller.outCashier);

module.exports = router;
