const Table = require(`../models/Table.model`);

exports.getAll = (req, res, next) => {
  var options = JSON.parse(req.query.options);
  var query =
    options.search != "" ? { tableId: { $regex: options.search } } : {};
  Table.paginate(query, options)
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(400).json({
        message: "ไม่สามารถเรียกดูข้อมูลโต๊ะได้ในขณะนี้, กรุณาลองใหม่อีกครั้ง",
        success: false,
        err: err
      });
    });
};


exports.add = (req, res, next) => {
  delete req.body.item._id;
  Table.create(req.body.item)
    .then(result => {
      res.status(200).json({
        success: true,
        data: result,
        message: "เพิ่มข้อมูลสำเร็จ"
      });
    })
    .catch(err => {
      //duplicate
      if (err.code === 11000) {
        res.status(200).json({
          success: false,
          message: "รหัสโต๊ะซ้ำ"
        });
      } else {
        res.status(400).json({
          success: false,
          message: "เพิ่มข้อมูลไม่สำเร็จ",
          err: err
        });
      }
    });
};

exports.edit = (req, res, next) => {
  Table.findByIdAndUpdate(req.params.id, req.body.item)
    .then(result => {
      res.status(200).json({
        success: true,
        data: result,
        message: "แก้ไขข้อมูลสำเร็จ"
      });
    })
    .catch(err => {
      if (err.keyValue) {
        var keyValue = JSON.stringify(err.keyValue).split(":");
        var key = keyValue[0]
          .substring(2, keyValue[0].length - 1)
          .toUpperCase();
        var value = keyValue[1].substring(1, keyValue[1].length - 2);
      }
      var array = err.message.split(" ");
      var message = "";
      for (let index = 4; index < array.length; index++) {
        const element = array[index];
        message += `${element} `;
      }
      message = message.slice(0, -1);

      //validate error code
      if (err.code === 11000) {
        message = `${key} ${value} ถูกใช้งานเรียบร้อยแล้ว`;
      }
      res.status(200).json({
        success: false,
        message: message
      });
    });
};

exports.delete = (req, res, next) => {
  Table.findByIdAndRemove(req.params.id)
    .then(() => {
      res.status(200).json({
        success: true,
        message: "ลบข้อมูลสำเร็จ"
      });
    })
    .catch(() => {
      res.status(400).json({
        success: false,
        message: "ไม่สามารถลบข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
      });
    });
};
