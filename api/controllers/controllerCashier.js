const Cashier = require("../models/Cashier.model");
const CashierLog = require("../models/CashierLog.model");

exports.getCashier = (req, res, next) => {
  Cashier.findOne({ _id: "main" })
    .populate("cashier", { firstName: 1, lastName: 1 })
    .then(result => {
      if (result) {
        res.status(200).json({
          success: true,
          data: result,
          message: "เรียกดูแคชเชียร์ที่เข้างานสำเร็จ"
        });
      } else {
        res.status(200).json({
          success: true,
          data: [],
          message: "ไม่มีแคชเชียร์ที่เข้างานอยู่ขณะนี้"
        });
      }
    })
    .catch(err => {
      res.status(400).json({
        message: "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้งในภายหลัง",
        success: false,
        err: err
      });
    });
};

exports.getAllCashierLog = (req, res, next) => {
  let query = {};
  let options = {};
  if (req.query.search) query.search = req.query.search;
  if (req.query.dateStart && req.query.dateEnd) {
    query.dateTime = {
      $gte: new Date(req.query.dateStart),
      $lt: new Date(req.query.dateEnd)
    };
  }
  
  if (req.query.sort) options.sort = req.query.sort;
  if (req.query.page) options.page = req.query.page;
  if (req.query.limit) options.limit = req.query.limit;
  options.sort = '-dateTime'
  options.populate = 'cashier'

  CashierLog.paginate(query, options)
    .then(result => {
      res.status(200).json({
        success: true,
        data: result,
        message: "เรียกดูข้อมูลประวัติแคชเชียร์สำเร็จ"
      });
    })
    .catch(err => {
      res.status(400).json({
        message: "ไม่สามารถเรียกดูข้อมูลโต๊ะได้ในขณะนี้, กรุณาลองใหม่อีกครั้ง",
        success: false,
        err: err
      });
    });
};

exports.attendCashier = (req, res, next) => {
  console.log(req.body.item);
  
  let item = {
    cashier: req.body.item.cashier,
    initialMoney: req.body.item.initialMoney
  };

  Cashier.findOne({ _id: "main" })
    .then(result => {
      if (result == null) {
        Cashier.findByIdAndUpdate("main", item, {
          upsert: true
        })
          .then(result => {
            res.status(200).json({
              success: true,
              message: "แคชเชียร์เข้ากะสำเร็จ"
            });
          })
          .catch(err => {
            res.status(400).json({
              message: "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้งในภายหลัง",
              success: false,
              err: err
            });
          });
      } else {
        if (result.cashier == null) {
          Cashier.findByIdAndUpdate("main", item, {
            upsert: true
          })
            .then(result => {
              res.status(200).json({
                success: true,
                message: "แคชเชียร์เข้ากะสำเร็จ"
              });
            })
            .catch(err => {
              res.status(400).json({
                message: "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้งในภายหลัง",
                success: false,
                err: err
              });
            });
        } else {
          res.status(200).json({
            success: false,
            message: "มีพนักงานเข้ากำลังเข้ากะอยู่ขณะนี้"
          });
        }
      }
    })
    .catch(err => {
      res.status(400).json({
        message: "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้งในภายหลัง",
        success: false,
        err: err
      });
    });
};

exports.outCashier = (req, res, next) => {
  let stopMoney = req.body.item.stopMoney;

  Cashier.findById("main")
    .then(cashier => {
      let logValue = {
        cashier: cashier.cashier,
        startMoney: cashier.initialMoney,
        stopMoney: stopMoney,
        orderValue: cashier.orderValue
      };

      console.log(logValue);

      CashierLog.create(logValue)
        .then(log => {
          Cashier.findByIdAndUpdate("main", {
            cashier: null,
            initialMoney: 0,
            orderValue: 0
          })
            .then(result => {
              res.status(200).json({
                success: true,
                message: "แคชเชียร์ออกกะสำเร็จ"
              });
            })
            .catch(err => {
              console.log(err);
              res.status(400).json({
                message: "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้งในภายหลัง",
                success: false,
                err: err
              });
            });
        })
        .catch(err => {
          console.log(err);
          res.status(400).json({
            message: "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้งในภายหลัง",
            success: false,
            err: err
          });
        });
    })
    .catch(err => {
      console.log(err);

      res.status(400).json({
        message: "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้งในภายหลัง",
        success: false,
        err: err
      });
    });
};
