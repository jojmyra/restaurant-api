const System = require("../models/System.model");

exports.config = (req, res, next) => {
  System.findByIdAndUpdate("mainshop", req.body.item, {
    upsert: true
  })
    .then(result => {
      System.findById("mainshop")
        .then(result => {
          res.status(200).json({
            success: true,
            data: result,
            message: "ตั้งค่าข้อมูลร้านสำเร็จ"
          });
        })
        .catch(err => {
          res.status(400).json({
            message: "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้ง",
            success: false,
            err: err
          });
        });
    })
    .catch(err => {
      res.status(400).json({
        message: "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้ง",
        success: false,
        err: err
      });
    });
};

exports.get = (req, res, next) => {
  System.findById("mainshop")
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(400).json({
        message: "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้ง",
        success: false,
        err: err
      });
    });
};
