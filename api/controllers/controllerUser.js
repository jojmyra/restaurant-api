const User = require("../models/User.model");
const bcrypt = require("bcrypt");
const saltRounds = 10;
const jwt = require("jsonwebtoken");
const config = require("config");
const privateKey = config.get("privatekey");

try {
  bcrypt.genSalt(saltRounds, function(err, salt) {
    bcrypt.hash("test123456", salt, function(err, hash) {
      let password = hash;
      User.create({
        username: "manager",
        password: password,
        prefixName: "นาย",
        firstName: "ชื่อจริง",
        lastName: "นามสกุล",
        telephone: "1234567890",
        email: "email@email.com",
        role: "ผู้จัดการ"
      })
        .then(result => {
          console.log("initial manager successfully");
        })
        .catch(err => {
          if (err.keyValue) {
            var keyValue = JSON.stringify(err.keyValue).split(":");
            var key = keyValue[0]
              .substring(2, keyValue[0].length - 1)
              .toUpperCase();
            var value = keyValue[1].substring(1, keyValue[1].length - 2);
          }
          var array = err.message.split(" ");
          var message = "";
          for (let index = 4; index < array.length; index++) {
            const element = array[index];
            message += `${element} `;
          }
          message = message.slice(0, -1);

          //validate error code
          if (err.code === 11000) {
            message = `${key} ${value} ถูกใช้งานเรียบร้อยแล้ว`;
          }
        });
    });
  });
} catch (error) {
  console.log(error);
}

exports.getAll = (req, res, next) => {
  var options = JSON.parse(req.query.options);
  var query =
    options.search != "" ? { username: { $regex: options.search } } : {};
  User.paginate(query, options)
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(400).json({
        message:
          "ไม่สามารถเรียกดูข้อมูลผู้ใช้งานได้ในขณะนี้, กรุณาลองใหม่อีกครั้ง",
        success: false,
        err: err
      });
    });
};

exports.add = (req, res, next) => {
  delete req.body.item._id;
  bcrypt.genSalt(saltRounds, function(err, salt) {
    bcrypt.hash(req.body.item.password, salt, function(err, hash) {
      req.body.item.password = hash;
      User.create(req.body.item)
        .then(result => {
          res.status(200).json({
            success: true,
            data: result,
            message: "เพิ่มข้อมูลสำเร็จ"
          });
        })
        .catch(err => {
          if (err.keyValue) {
            var keyValue = JSON.stringify(err.keyValue).split(":");
            var key = keyValue[0]
              .substring(2, keyValue[0].length - 1)
              .toUpperCase();
            var value = keyValue[1].substring(1, keyValue[1].length - 2);
          }
          var array = err.message.split(" ");
          var message = "";
          for (let index = 4; index < array.length; index++) {
            const element = array[index];
            message += `${element} `;
          }
          message = message.slice(0, -1);

          //validate error code
          if (err.code === 11000) {
            message = `${key} ${value} ถูกใช้งานเรียบร้อยแล้ว`;
          }
          res.status(200).json({
            success: false,
            message: message
          });
        });
    });
  });
};

exports.edit = (req, res, next) => {
  delete req.body.item.username;
  delete req.body.item.password;

  User.findByIdAndUpdate(req.params.id, req.body.item)
    .then(result => {
      res.status(200).json({
        success: true,
        data: result,
        message: "แก้ไขข้อมูลสำเร็จ"
      });
    })
    .catch(err => {
      if (err.keyValue) {
        var keyValue = JSON.stringify(err.keyValue).split(":");
        var key = keyValue[0]
          .substring(2, keyValue[0].length - 1)
          .toUpperCase();
        var value = keyValue[1].substring(1, keyValue[1].length - 2);
      }
      var array = err.message.split(" ");
      var message = "";
      for (let index = 4; index < array.length; index++) {
        const element = array[index];
        message += `${element} `;
      }
      message = message.slice(0, -1);

      //validate error code
      if (err.code === 11000) {
        message = `${key} ${value} ถูกใช้งานเรียบร้อยแล้ว`;
      }
      res.status(200).json({
        success: false,
        message: message
      });
    });
};

exports.delete = (req, res, next) => {
  User.findByIdAndRemove(req.params.id)
    .then(() => {
      res.status(200).json({
        success: true,
        message: "ลบข้อมูลสำเร็จ"
      });
    })
    .catch(() => {
      res.status(400).json({
        success: false,
        message: "ไม่สามารถลบข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
      });
    });
};

exports.refreshToken = (req, res, next) => {
  console.log(req.body);
  try {
    jwt.verify(req.body.accessToken, privateKey)
  } catch (error) {
    
  }
}

exports.login = (req, res, next) => {
  var check = false;
  var message = "กรุณาส่งข้อมูลให้ครบถ้วน";
  if (req.body.item === undefined) {
    check = true;
    message = "กรุณาส่งข้อมูลให้ครบถ้วน";
  } else {
    if (req.body.item.userDetails === undefined) {
      check = true;
      message = "กรุณาส่งข้อมูลให้ครบถ้วน";
    } else {
      if (req.body.item.userDetails.username === undefined) {
        check = true;
        message = `${message}, รหัสผู้ใช้`;
      }
      if (req.body.item.userDetails.password === undefined) {
        check = true;
        message = `${message}, รหัสผ่าน`;
      }
    }
  }

  if (check) {
    res.status(200).json({
      success: false,
      message: message
    });
  } else {
    User.findOne({
      username: req.body.item.userDetails.username
    })
      .then(User => {
        if (User) {
          // มี user
          bcrypt
            .compare(req.body.item.userDetails.password, User.password)
            .then(passwordMatch => {
              if (passwordMatch) {
                var token = jwt.sign(
                  {
                    _id: User._id,
                    authorized: true
                  },
                  privateKey,
                  { expiresIn: "1 days" }
                );
                jwt.verify(token, privateKey, (err, auth) => {
                  if (err) res.statusCode(403);
                  res.status(200).json({
                    success: true,
                    accessToken: token,
                    AppActiveUser: {
                      id: User._id,
                      about: User.username,
                      displayName: `${User.firstName} ${User.lastName}`,
                      userRole: User.role
                    },
                    tokenExpiry: auth.exp * 1000,
                    message: `เข้าสู่ระบบสำเร็จ`
                  });
                });
              } else {
                res.status(200).json({
                  success: false,
                  message: `รหัสผ่านผิดพลาด`
                });
              }
            })
            .catch(err => {
              res.status(400).json(err);
            });
        } else {
          // ไม่มี user
          res.status(200).json({
            success: false,
            message: `ไม่มีรหัสผู้ใช้งานนี้ในระบบ`
          });
        }
      })
      .catch(err => {
        res.status(400).json(err);
      });
  }
};

exports.changePassword = (req, res, next) => {
  console.log(req.body.item);

  User.findOne({
    username: req.body.item.username
  })
    .then(User => {
      console.log(User);

      if (User) {
        // ถ้าเจอ user
        bcrypt.compare(
          req.body.item.oldPassword,
          User.password,
          (err, result) => {
            console.log(result);

            if (result) {
              // รหัสผ่านเก่าถูกต้อง
              bcrypt.genSalt(saltRounds, function(err, salt) {
                bcrypt.hash(req.body.item.newPassword, salt, function(
                  err,
                  hash
                ) {
                  // เข้ารหัสรหัสผ่านใหม่
                  if (!err) {
                    User.password = hash;
                    User.save()
                      .then(result => {
                        res.status(200).json({
                          success: true,
                          message: "เปลี่ยนรหัสผ่านสำเร็จ"
                        });
                      })
                      .catch(err => {
                        console.log(err);

                        res.status(400).json({
                          success: false,
                          message: "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้ง"
                        });
                      });
                  }
                });
              });
            } else {
              // รหัสเก่าผิดพลาด
              res.status(200).json({
                success: false,
                message: "เปลี่ยนรหัสผ่านไม่สำเร็จ, รหัสผ่านเดิมผิดพลาด"
              });
            }
          }
        );
      } else {
        // ถ้าไม่เจอ user
        res.status(200).json({
          success: false,
          message: "ไม่เจอรหัสผู้ใช้งานนี้"
        });
      }
    })
    .catch(err => {
      res.status(400).json({
        success: false,
        message: "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้ง"
      });
    });
};

exports.getCashier = (req, res, next) => {
  let name = req.query.name ? req.query.name : "";

  User.aggregate([
    {
      $match: {
        firstName: {
          $regex: name
        },
        role: "แคชเชียร์"
      }
    },
    {
      $project: {
        _id: 1,
        fullName: {
          $concat: ["$firstName", " ", "$lastName"]
        }
      }
    }
  ])
    .then(result => {
      res.status(200).json({
        success: true,
        data: result,
        message: "เรียกข้อมูลแคชเชียร์สำเร็จ"
      });
    })
    .catch(err => {
      res.status(400).json({
        success: false,
        message: "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้ง",
        err: err
      });
    });

  // User.find({
  //   firstName: {
  //     $regex: name
  //   }
  // })
  //   .then(result => {
  //     res.status(200).json({
  //       success: true,
  //       data: result,
  //       message: "เรียกข้อมูลแคชเชียร์สำเร็จ"
  //     });
  //   })
  //   .catch(err => {
  //     res.status(400).json({
  //       success: false,
  //       message: "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้ง",
  //       err: err
  //     });
  //   });
};
