const { WebhookClient, Payload } = require("dialogflow-fulfillment");
const Line = require("../models/LineClient.model");
const Table = require("../models/Table.model");
const FoodMenu = require("../models/FoodMenu.model");
const FoodMenuItem = require("../models/FoodMenuItem.model");
const Order = require("../models/Order.model");

exports.defaultFallBack = (request, response, next) => {
  //   console.log(request.body);
  //   console.log(request.body.queryResult.intent.displayName);

  const receivedPayload = request.body.originalDetectIntentRequest.payload;
  const userId = receivedPayload.data.source.userId;
  let lineQuery = {
    userId: userId
  };

  const message = receivedPayload.data.message.text;

  let intentMap = new Map();

  const agent = new WebhookClient({ request, response });

  switch (request.body.queryResult.intent.displayName) {
    case "Default":
      break;
    case "Login - yes":
      Login();
      break;
    case "Order - custom - yes":
      OrderYes();
      break;
    case "OrderStatus":
      OrderStatus();
      break;
    case "OrderValue":
      OrderValue();
      break;
    case "logOut":
      logOut();
      break;
  }

  function error(agent) {
    agent.add("เกิดข้อผิดพลาด");
    agent.add("กรุณาลองใหม่อีกครั้งในภายหลังนะคะ");
  }

  function unLogged(agent) {
    agent.add("กรุณาใส่รหัสเพื่อเข้าสู่โต๊ะก่อนนะคะ");
    agent.add("ใส่รหัสดังรูปแบบต่อไปนี้");
    agent.add("เข้าโต๊ะ 'รหัสเข้าโต๊ะ' (ไม่ต้องมีฟันหนู)");
  }

  function Login() {
    const accessCode = request.body.queryResult.parameters.accessCode;
    function logginSuccess(agent) {
      agent.add("เข้าสู่โต๊ะสำเร็จ");
      agent.add("กรุณาพิมพ์เมนูที่ต้องการทำ");
      agent.add("- สั่งอาหาร");
      agent.add("- ดูสถานะ");
      agent.add("- ยกเลิกอาหาร");
    }
    function logginAlready(agent) {
      agent.add("มีโต๊ะที่เชื่อมกับไลน์นี้เรียบร้อยแล้ว");
      agent.add("กรุณาพิมพ์เมนูที่ต้องการทำ");
      agent.add("- สั่งอาหาร");
      agent.add("- ดูสถานะ");
      agent.add("- ยกเลิกอาหาร");
    }
    function acessWrong(agent) {
      agent.add("รหัสเข้าโต๊ะผิดพลาด");
      agent.add("กรุณาใส่รหัสเข้าโต๊ะใหม่นะคะ");
    }

    Line.findOne(lineQuery)
      .then(result => {
        if (result) {
          if (result.accessCode) {
            // already login
            intentMap.set("Login - yes", logginAlready);
            agent.handleRequest(intentMap);
          } else {
            // unlogged
            Table.findOne({
              acessCode: accessCode
            })
              .then(result => {
                if (result) {
                  Line.findOneAndUpdate(
                    {
                      userId: lineQuery.userId
                    },
                    {
                      userId: lineQuery.userId,
                      tableId: result._id
                    },
                    { upsert: true }
                  )
                    .then(result => {
                      intentMap.set("Login - yes", logginSuccess);
                      agent.handleRequest(intentMap);
                    })
                    .catch(err => {
                      intentMap.set("Login - yes", error);
                      agent.handleRequest(intentMap);
                    });
                } else {
                  intentMap.set("Login - yes", acessWrong);
                  agent.handleRequest(intentMap);
                }
              })
              .catch(err => {
                intentMap.set("Login - yes", error);
                agent.handleRequest(intentMap);
              });
          }
        } else {
          Table.findOne({
            acessCode: accessCode
          })
            .then(result => {
              if (result) {
                Line.findOneAndUpdate(
                  {
                    userId: lineQuery.userId
                  },
                  {
                    userId: lineQuery.userId,
                    tableId: result._id
                  },
                  { upsert: true }
                )
                  .then(result => {
                    intentMap.set("Login - yes", logginSuccess);
                    agent.handleRequest(intentMap);
                  })
                  .catch(err => {
                    intentMap.set("Login - yes", error);
                    agent.handleRequest(intentMap);
                  });
              } else {
                intentMap.set("Login - yes", acessWrong);
                agent.handleRequest(intentMap);
              }
            })
            .catch(err => {
              intentMap.set("Login - yes", error);
              agent.handleRequest(intentMap);
            });
        }
      })
      .catch(err => {
        intentMap.set("Login - yes", error);
        agent.handleRequest(intentMap);
      });
  }

  function OrderYes() {
    const foodName = request.body.queryResult.parameters.foodName;
    function notFound(agent) {
      agent.add("ขออภัยค่ะ");
      agent.add(`ทางร้านไม่มีเมนูชื่อ ${foodName} นะคะ`);
    }

    function orderSuccess(agent) {
      agent.add("สั่งเมนูสำเร็จแล้ว");
      agent.add("รอเมนูสักครู่นะ :)");
    }

    Line.findOne(lineQuery)
      .then(client => {
        if (client) {
          console.log(client);

          // มีข้resultอมูลในฐานข้อมูล
          if (client.tableId) {
            FoodMenu.findOne({
              foodName: foodName
            })
              .then(food => {
                if (food) {
                  // มีเมนู
                  Table.findById(client.tableId)
                    .then(table => {
                      FoodMenuItem.create({
                        quantity: 1,
                        sumPrice: food.price,
                        tableId: table.tableId,
                        foodName: food.foodName,
                        foodType: food.foodType,
                        price: food.price,
                        calroies: food.calroies,
                        user: "lineUser"
                      })
                        .then(menuItem => {
                          Order.findByIdAndUpdate(table.order, {
                            $push: {
                              menu: menuItem
                            }
                          })
                            .then(result => {
                              intentMap.set(
                                "Order - custom - yes",
                                orderSuccess
                              );
                              agent.handleRequest(intentMap);
                            })
                            .catch(err => {
                              console.log(err);

                              intentMap.set("Order - custom - yes", error);
                              agent.handleRequest(intentMap);
                            });
                        })
                        .catch(err => {
                          console.log(err);

                          intentMap.set("Order - custom - yes", error);
                          agent.handleRequest(intentMap);
                        });
                    })
                    .catch(err => {
                      console.log(err);

                      intentMap.set("Order - custom - yes", error);
                      agent.handleRequest(intentMap);
                    });
                } else {
                  // ไม่มีเมนู
                  intentMap.set("Order - custom - yes", notFound);
                  agent.handleRequest(intentMap);
                }
              })
              .catch(err => {
                console.log(err);
                intentMap.set("Order - custom - yes", error);
                agent.handleRequest(intentMap);
              });
          } else {
            intentMap.set("Order - custom - yes", unLogged);
            agent.handleRequest(intentMap);
          }
        } else {
          intentMap.set("Order - custom - yes", unLogged);
          agent.handleRequest(intentMap);
        }
      })
      .catch(err => {
        console.log(err);

        intentMap.set("Order - custom - yes", error);
        agent.handleRequest(intentMap);
      });
  }

  function OrderStatus() {
    function notFound(agent) {
      agent.add("โต๊ะที่ลูกค้าเชื่อมต่ออยู่นั้น ไม่มีในระบบ");
      agent.add("กรุณากดออกจากโต๊ะ และเชื่อมต่อใหม่อีกครั้งค่ะ");
    }

    Line.findOne(lineQuery)
      .then(client => {
        if (client) {
          // มี client ในฐานข้อมูลแล้ว
          if (client.tableId) {
            // มีโต๊ะที่เชื่อมแล้ว
            Table.findById(client.tableId)
              .then(table => {
                if (table) {
                  // หาโต๊ะเจอ
                  Order.findById(table.order)
                    .populate("menu")
                    .then(order => {
                      function numberWithCommas(x) {
                        return x
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                      }

                      let menu = order.menu.sort((a, b) => {
                        return a.status.localeCompare(b.status, "th");
                      });

                      function orderShow() {
                        menu.forEach(item => {
                          agent.add(`${item.foodName} ${item.status}`);
                        });
                      }

                      intentMap.set("OrderStatus", orderShow);

                      agent.handleRequest(intentMap);
                    })
                    .catch(err => {
                      intentMap.set("OrderStatus", error);
                      agent.handleRequest(intentMap);
                    });
                } else {
                  // หาโต๊ะไม่เจอ
                  intentMap.set("OrderStatus", notFound);
                  agent.handleRequest(intentMap);
                }
              })
              .catch(err => {
                intentMap.set("OrderStatus", error);
                agent.handleRequest(intentMap);
              });
          } else {
            // ยังไม่มีโต๊ะที่เชื่อม
            intentMap.set("OrderStatus", unLogged);
            agent.handleRequest(intentMap);
          }
        } else {
          // ยังไม่มี client ในฐานข้อมูล
          intentMap.set("OrderStatus", unLogged);
          agent.handleRequest(intentMap);
        }
      })
      .catch(err => {
        intentMap.set("OrderStatus", error);
        agent.handleRequest(intentMap);
      });
  }

  function OrderValue() {
    function notFound(agent) {
      agent.add("โต๊ะที่ลูกค้าเชื่อมต่ออยู่นั้น ไม่มีในระบบ");
      agent.add("กรุณากดออกจากโต๊ะ และเชื่อมต่อใหม่อีกครั้งค่ะ");
    }

    Line.findOne(lineQuery)
      .then(client => {
        if (client) {
          // มี client ในฐานข้อมูลแล้ว
          if (client.tableId) {
            // มีโต๊ะที่เชื่อมแล้ว
            Table.findById(client.tableId)
              .then(table => {
                if (table) {
                  // หาโต๊ะเจอ
                  Order.findById(table.order)
                    .populate("menu")
                    .then(order => {
                      function numberWithCommas(x) {
                        return x
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                      }

                      let menu = order.menu.sort((a, b) => {
                        return a.status.localeCompare(b.status, "th");
                      });
                      let sum = menu.reduce((sum, x) => (sum += x.price), 0);
                      sum = numberWithCommas(sum);

                      function orderShow() {
                        agent.add(`ยอดรวมออเดอร์ทั้งหมด ${sum} บาท`);
                      }

                      intentMap.set("OrderValue", orderShow);
                      agent.handleRequest(intentMap);
                    })
                    .catch(err => {
                      intentMap.set("OrderValue", error);
                      agent.handleRequest(intentMap);
                    });
                } else {
                  // หาโต๊ะไม่เจอ
                  intentMap.set("OrderValue", notFound);
                  agent.handleRequest(intentMap);
                }
              })
              .catch(err => {
                intentMap.set("OrderValue", error);
                agent.handleRequest(intentMap);
              });
          } else {
            // ยังไม่มีโต๊ะที่เชื่อม
            intentMap.set("OrderValue", unLogged);
            agent.handleRequest(intentMap);
          }
        } else {
          // ยังไม่มี client ในฐานข้อมูล
          intentMap.set("OrderValue", unLogged);
          agent.handleRequest(intentMap);
        }
      })
      .catch(err => {
        intentMap.set("OrderValue", error);
        agent.handleRequest(intentMap);
      });
  }

  function logOut() {
    function logOutSuccess(agent) {
      agent.add("ลูกค้าได้ยุติการเชื่อมต่อกับโต๊ะเรียบร้อยแล้ว");
      agent.add("ขอบคุณที่ใช้บริการนะคะ");
    }
    Line.findOne(lineQuery)
      .then(client => {
        if (client) {
          // มีในฐานข้อมูล
          if (client.tableId) {
            Line.findOneAndUpdate(lineQuery, {
              tableId: null
            })
              .then(result => {
                intentMap.set("logOut", logOutSuccess);
                agent.handleRequest(intentMap);
              })
              .catch(err => {
                intentMap.set("logOut", error);
                agent.handleRequest(intentMap);
              });
          } else {
            intentMap.set("logOut", unLogged);
            agent.handleRequest(intentMap);
          }
        } else {
          // ไม่มีในฐานข้อมูล
          intentMap.set("logOut", unLogged);
          agent.handleRequest(intentMap);
        }
      })
      .catch(err => {
        intentMap.set("logOut", error);
        agent.handleRequest(intentMap);
      });
  }
};
