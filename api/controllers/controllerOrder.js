const Bill = require("../models/Bill.model");
const User = require("../models/User.model");
const Cashier = require("../models/Cashier.model");
const Order = require("../models/Order.model");
const Table = require("../models/Table.model");
const FoodMenuItem = require("../models/FoodMenuItem.model");
const moment = require("moment");

function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

exports.get = (req, res, next) => {
  Order.findById(req.params.id)
    .populate("menu")
    .then(result => {
      Table.findOne({
        tableId: result.table
      })
        .then(table => {
          const obj = Object.assign({}, result._doc);
          obj.acessCode = table.acessCode;

          res.status(200).json({
            success: true,
            data: obj,
            message: "เรียกดูข้อมูลสำเร็จ"
          });
        })
        .catch(err => { });
    })
    .catch(err => {
      res.status(400).json({
        success: false,
        message: "ไม่สามารถเรียกดูข้อมูลผู้ใช้งานได้ในขณะนี้, กรุณาลองใหม่อีกครั้ง",
        err: err
      });
    });
};

exports.add = (req, res, next) => {
  Table.findOne({
    tableId: req.body.item.orders.tableId
  })
    .then(table => {
      if (table.status === "ว่าง") {
        // ถ้าโต๊ะว่างเพิ่มทั้งหมด
        FoodMenuItem.insertMany(req.body.item.orders.menu)
          .then(result => {
            // res.status(200).json(result)
            Order.create({
              table: req.body.item.orders.tableId,
              customerNumber: req.body.item.orders.customerNumber,
              menu: result
            })
              .then(order => {
                let tableToken = makeid(6);
                Table.findOneAndUpdate({
                  tableId: req.body.item.orders.tableId
                }, {
                  status: "ไม่ว่าง",
                  order: order._id,
                  acessCode: tableToken
                })
                  .then(result => {
                    res.status(200).json({
                      success: true,
                      data: order._id,
                      message: "เพิ่มรายการอาหารสำเร็จ",
                      token: tableToken
                    });
                  })
                  .catch(err => {
                    res.status(400).json({
                      success: false,
                      message: "ไม่สามารถเพิ่มข้อมูลได้, กรุณาลองใหม่อีกครั้ง",
                      err: err
                    });
                  });
              })
              .catch(err => {
                console.log(err);

                res.status(400).json({
                  success: false,
                  message: "ไม่สามารถเพิ่มข้อมูลได้, กรุณาลองใหม่อีกครั้ง",
                  err: err
                });
              });
          })
          .catch(err => {
            console.log(err);

            res.status(400).json({
              success: false,
              message: "ไม่สามารถเพิ่มข้อมูลได้, กรุณาลองใหม่อีกครั้ง",
              err: err
            });
          });
      } else {
        // ถ้าโต๊ะไม่ว่างเพิ่มเฉพาะเมนู
        FoodMenuItem.insertMany(req.body.item.orders.menu)
          .then(result => {
            Order.findByIdAndUpdate(table.order, {
              $push: {
                menu: result
              }
            })
              .then(order => {
                res.status(200).json({
                  success: true,
                  data: order._id,
                  message: "เพิ่มรายการอาหารสำเร็จ"
                });
              })
              .catch(err => {
                res.status(400).json({
                  success: false,
                  message: "ไม่สามารถเพิ่มข้อมูลได้, กรุณาลองใหม่อีกครั้ง",
                  err: err
                });
              });
          })
          .catch(err => {
            console.log(err);

            res.status(400).json({
              success: false,
              message: "ไม่สามารถเพิ่มข้อมูลได้, กรุณาลองใหม่อีกครั้ง",
              err: err
            });
          });
      }
    })
    .catch(err => {
      console.log(err);

      res.status(400).json({
        success: false,
        message: "ไม่สามารถเพิ่มข้อมูลได้, กรุณาลองใหม่อีกครั้ง",
        err: err
      });
    });
};

exports.cancelOrder = (req, res, next) => {
  let cancel = {
    by: req.body.item.cancelBy,
    number: req.body.item.cancelQuantity,
    user: req.body.item.user
  };
  Order.findOneAndUpdate({
    menu: req.body.item._id
  }, {
    $pull: {
      menu: req.body.item._id
    }
  })
    .then(result => {
      console.log(result);
      FoodMenuItem.findByIdAndUpdate(req.body.item._id, {
        status: "ยกเลิก",
        cancelOrder: cancel,
        holdOrder: null
      })
        .then(result => {
          console.log(result);

          res.status(200).json({
            success: true,
            message: "ยกเลิกออเดอร์สำเร็จ"
          });
        })
        .catch(err => {
          res.status(400).json({
            success: false,
            message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง",
            err: err
          });
        });
    })
    .catch(err => {
      console.log(err);

      res.status(400).json({
        success: false,
        message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง",
        err: err
      });
    });

  // FoodMenuItem.findById(req.body.item._id)
  //   .then(result => {
  //     var newQuantity = result.quantity - req.body.item.cancelQuantity;
  //     var newSumPrice =
  //       result.sumPrice - result.price * req.body.item.cancelQuantity;
  //     var cancel = {
  //       by: req.body.item.cancelBy,
  //       number: req.body.item.cancelQuantity,
  //       user: req.body.item.user
  //     };

  //     if (newQuantity === 0 || newSumPrice === 0) {
  //       Order.findByIdAndUpdate(req.body.item.orderId, {
  //         $pull: { menu: req.body.item._id }
  //       })
  //         .then(order => {
  //           FoodMenuItem.findByIdAndUpdate(req.body.item._id, {
  //             status: "ยกเลิก",
  //             quantity: newQuantity,
  //             sumPrice: newSumPrice,
  //             cancelOrder: cancel
  //           })
  //             .then(result => {
  //               res.status(200).json({
  //                 success: true,
  //                 message: "ยกเลิกออเดอร์สำเร็จ"
  //               });
  //             })
  //             .catch(err => {
  //               res.status(400).json({
  //                 success: false,
  //                 message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
  //               });
  //             });
  //         })
  //         .catch(err => {
  //           res.status(400).json({
  //             success: false,
  //             message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
  //           });
  //         });
  //     } else {
  //       FoodMenuItem.findByIdAndUpdate(req.body.item._id, {
  //         quantity: newQuantity,
  //         sumPrice: newSumPrice,
  //         cancelOrder: cancel
  //       })
  //         .then(result => {
  //           res.status(200).json({
  //             success: true,
  //             message: "ยกเลิกออเดอร์สำเร็จ"
  //           });
  //         })
  //         .catch(err => {
  //           res.status(400).json({
  //             success: false,
  //             message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
  //           });
  //         });
  //     }
  //   })
  //   .catch(err => {
  //     res.status(400).json({
  //       success: false,
  //       message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
  //     });
  //   });
};

exports.cancelSuccess = (req, res, next) => {
  let cancel = {
    by: req.body.item.cancelBy,
    number: req.body.item.cancelQuantity,
    user: req.body.item.user
  };
  if (req.body.item.status !== "ยกเลิก") {
    FoodMenuItem.findByIdAndUpdate(req.body.item._id, {
      status: "ยกเลิก",
      cancelOrder: cancel
    })
      .then(result => {
        res.status(200).json({
          success: true,
          message: "ยกเลิกออเดอร์สำเร็จ"
        });
      })
      .catch(err => {
        res.status(400).json({
          success: false,
          message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
        });
      });
  } else {
    FoodMenuItem.findByIdAndUpdate(req.body.item._id, {
      status: "รายการสำเร็จ",
      cancelOrder: null
    })
      .then(result => {
        res.status(200).json({
          success: true,
          message: "ยกเลิก การยกเลิกออเดอร์สำเร็จ"
        });
      })
      .catch(err => {
        res.status(400).json({
          success: false,
          message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
        });
      });
  }
};

exports.serve = (req, res, next) => {
  FoodMenuItem.findByIdAndUpdate(req.body.item._id, {
    status: "รายการสำเร็จ",
    serve: req.body.item.serve,
    holdOrder: null
  })
    .then(result => {
      res.status(200).json({
        success: true,
        message: "ส่งออเดอร์สำเร็จ"
      });
    })
    .catch(err => {
      res.status(400).json({
        success: false,
        message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
      });
    });
};

exports.changeTable = (req, res, next) => {
  Table.findOne({
    tableId: req.body.item.destinationTable
  })
    .then(result => {
      if (result.status === "ว่าง" || result.order === null) {
        // แก้โต๊ะปลายทางให้อัพเดทฟิลด์ออเดอร์ และเปลี่ยนสถานะเป็นไม่ว่าง
        Table.findOneAndUpdate({
          tableId: req.body.item.destinationTable
        }, {
          order: req.body.item.orderId,
          status: "ไม่ว่าง",
          acessCode: req.body.item.acessCode
        })
          .then(result => {
            // แก้ฟิลด์ table ให้เป็นโต๊ะที่ย้ายไป
            Order.findByIdAndUpdate(req.body.item.orderId, {
              table: req.body.item.destinationTable
            })
              .then(result => {
                // แก้สถานะโต๊ะเก่าเปลี่ยนเป็นว่าง และเอาข้อมูลออเดอร์ออก
                Table.findOneAndUpdate({
                  tableId: req.body.item.oldTable
                }, {
                  status: "ว่าง",
                  order: null
                })
                  .then(result => {
                    Order.findById(req.body.item.orderId)
                      .then(result => {
                        FoodMenuItem.updateMany({
                          _id: {
                            $in: result.menu
                          }
                        }, {
                          tableId: req.body.item.destinationTable
                        })
                          .then(result => {
                            res.status(200).json({
                              success: true,
                              message: "ย้ายโต๊ะสำเร็จ"
                            });
                          })
                          .catch(err => {
                            res.status(400).json({
                              success: false,
                              message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
                            });
                          });
                      })
                      .catch(err => {
                        res.status(400).json({
                          success: false,
                          message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
                        });
                      });
                  })
                  .catch(err => {
                    res.status(400).json({
                      success: false,
                      message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
                    });
                  });
              })
              .catch(err => {
                res.status(400).json({
                  success: false,
                  message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
                });
              });
          })
          .catch(err => {
            res.status(400).json({
              success: false,
              message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
            });
          });
      } else {
        //โต๊ะไม่ว่าง
        res.status(200).json({
          success: false,
          message: "ไม่สามารถย้ายไปยังโต๊ะที่ไม่ว่างได้"
        });
      }
    })
    .catch(err => {
      res.status(400).json({
        success: false,
        message: "ไม่สามารถแก้ไขข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
      });
    });
};

exports.getOrderUndone = (req, res, next) => {
  var options = JSON.parse(req.query.options);
  options.sort = {
    time: 1
  };
  FoodMenuItem.paginate({
    status: "ได้รับรายการ"
  }, options)
    .then(result => {
      res.status(200).json({
        success: true,
        data: result,
        message: "เรียกดูข้อมูลสำเร็จ"
      });
    })
    .catch(err => {
      res.status(400).json({
        message: "ไม่สามารถเรียกดูข้อมูลผู้ใช้งานได้ในขณะนี้, กรุณาลองใหม่อีกครั้ง",
        success: false,
        err: err
      });
    });
};

exports.acceptKitchen = (req, res, next) => {
  console.log(req.body.item);

  FoodMenuItem.updateMany({
    _id: {
      $in: req.body.item.order
    }
  }, {
    $set: {
      status: "กำลังทำรายการ",
      kitchen: req.body.item.kitchen,
      holdOrder: req.body.item.userId
    }
  })
    .then(result => {
      console.log(result);

      res.status(200).json({
        success: true,
        data: result,
        message: "รับออเดอร์สำเร็จ"
      });
    })
    .catch(err => {
      res.status(400).json({
        success: false,
        message: "เกิดข้อผิดพลาด"
      });
    });

  // FoodMenuItem.findByIdAndUpdate(req.body.item.order, {
  //   holdOrder: req.body.item.userId
  // })
  //   .then(result => {
  //     FoodMenuItem.updateMany(
  //       {
  //         _id: { $in: req.body.item.order }
  //       },
  //       {
  //         $set: {
  //           status: "กำลังทำรายการ",
  //           kitchen: req.body.item.kitchen
  //         }
  //       }
  //     )
  //       .then(result => {
  //         res.status(200).json({
  //           success: true,
  //           data: result,
  //           message: "รับออเดอร์สำเร็จ"
  //         });
  //       })
  //       .catch(err => {
  //         res.status(400).json({
  //           success: false,
  //           message: "เกิดข้อผิดพลาด"
  //         });
  //       });
  //   })
  //   .catch(err => {
  //     res.status(400).json({
  //       success: false,
  //       message: "เกิดข้อผิดพลาด"
  //     });
  //   });
};

exports.getOrderHistory = (req, res, next) => {
  FoodMenuItem.find({
    holdOrder: req.params.id
  })
    .then(result => {
      res.status(200).json({
        success: true,
        data: result,
        message: "เรียกดูข้อมูลสำเร็จ"
      });
    })
    .catch(err => {
      res.status(400).json({
        success: false,
        message: "เกิดข้อผิดพลาด, กรุณาลองใหม่อีกครั้ง",
        err: err
      });
    });
};

exports.payment = (req, res, next) => {
  let obj = {
    table: req.body.item.table,
    totalMenu: req.body.item.totalMenu,
    totalPrice: req.body.item.totalPrice,
    recieved: req.body.item.recieved,
    change: req.body.item.change,
    order: req.body.item.order,
    cashier: req.body.item.cashier,
    cashierRef: req.body.item.cashierRef
  };

  Cashier.findOne({
    cashier: req.body.item.cashierRef
  })
    .then(result => {
      if (result) {
        Cashier.findOneAndUpdate({
          cashier: req.body.item.cashierRef
        }, {
          $inc: {
            orderValue: req.body.item.totalPrice
          }
        })
          .then(result => {
            Bill.create(obj)
              .then(bill => {
                Order.findByIdAndUpdate(req.body.item.order, {
                  orderStatus: "กระบวนการสำเร็จ"
                })
                  .then(order => {
                    Table.findOneAndUpdate({
                      tableId: req.body.item.table
                    }, {
                      status: "ว่าง",
                      order: null,
                      acessCode: null
                    })
                      .then(result => {
                        res.status(200).json({
                          success: true,
                          data: bill,
                          message: "ชำระเงินสำเร็จ"
                        });
                      })
                      .catch(err => {
                        res.status(400).json({
                          success: false,
                          message: "เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง",
                          err: err
                        });
                      });
                  })
                  .catch(err => {
                    res.status(400).json({
                      success: false,
                      message: "เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง",
                      err: err
                    });
                  });
              })
              .catch(err => {
                res.status(400).json({
                  success: false,
                  message: "เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง",
                  err: err
                });
              });
          })
          .catch(err => {
            res.status(400).json({
              success: false,
              message: "เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง",
              err: err
            });
          });
      } else {
        res.status(200).json({
          success: false,
          message: "กรุณาเลือกแคชเชียร์ที่ต้องการชำระเงินให้ถูกต้อง"
        });
      }
    })
    .catch(err => {
      res.status(400).json({
        success: false,
        message: "เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง",
        err: err
      });
    });
};

exports.report = (req, res, next) => {
  let dateStart = new Date(req.query.dateStart);
  dateStart.setUTCHours(24, 0, 0, 0);
  let dateEnd = new Date(req.query.dateEnd);
  dateEnd.setUTCHours(24, 0, 0, 0);

  let result = {};
  Bill.aggregate([
    {
      $match: {
        updatedAt: {
          $gte: dateStart,
          $lt: dateEnd
        }
      }
    },
    {
      $group: {
        _id: null,
        income: {
          $sum: "$totalPrice"
        },
        totalBill: {
          $sum: 1
        }
      }
    }
  ])
    .then(bill => {
      result.bill = bill;
      // จำนวนลูกค้า
      Order.aggregate([{
        $match: {
          orderStatus: "กระบวนการสำเร็จ",
          updatedAt: {
            $gte: dateStart,
            $lt: dateEnd
          }
        }
      },
      {
        $group: {
          _id: null,
          customerCount: {
            $sum: "$customerNumber"
          }
        }
      }
      ])
        .then(customerCount => {
          result.customer = customerCount;
          // ยอดยกเลิก
          FoodMenuItem.aggregate([{
            $match: {
              status: "ยกเลิก",
              updatedAt: {
                $gte: dateStart,
                $lt: dateEnd
              }
            }
          },
          {
            $group: {
              _id: null,
              cancelCount: {
                $sum: "$quantity"
              }
            }
          }
          ])
            .then(cancel => {
              result.cancel = cancel;
              // อาหารขายดี
              FoodMenuItem.aggregate([{
                $match: {
                  status: "รายการสำเร็จ",
                  updatedAt: {
                    $gte: dateStart,
                    $lt: dateEnd
                  }
                }
              },
              {
                $group: {
                  _id: "$foodName",
                  orderCount: {
                    $sum: "$quantity"
                  },
                  orderValue: {
                    $sum: "$sumPrice"
                  }
                }
              },
              {
                $sort: {
                  orderCount: -1
                }
              },
              {
                $limit: 5
              }
              ])
                .then(bestSell => {
                  result.bestSell = bestSell;
                  res.status(200).json({
                    success: true,
                    data: result,
                    message: "เรียกข้อมูลสำเร็จ"
                  });
                })
                .catch(err => {
                  res.status(404).json(err);
                });
            })
            .catch(err => {
              res.status(404).json(err);
            });
        })
        .catch(err => {
          res.status(404).json(err);
        });
    })
    .catch(err => {
      res.status(404).json(err);
    });
};

exports.reportCard = (req, res, next) => {
  let dateStart = new Date(req.query.dateStart);
  dateStart.setUTCHours(24, 0, 0, 0);
  let dateEnd = new Date(req.query.dateEnd);
  dateEnd.setUTCHours(24, 0, 0, 0);

  let result = {};
  Bill.aggregate([{
    $match: {
      updatedAt: {
        $gte: dateStart,
        $lt: dateEnd
      }
    }
  },
  {
    $project: {
      day: {
        $substr: ["$updatedAt", 0, 10]
      },
      totalPrice: 1
    }
  },
  {
    $group: {
      _id: "$day",
      income: {
        $sum: "$totalPrice"
      },
      totalBill: {
        $sum: 1
      }
    }
  },
  {
    $sort: {
      _id: 1
    }
  }
  ])
    .then(bill => {
      result.bill = bill;
      // จำนวนลูกค้า
      Order.aggregate([{
        $match: {
          orderStatus: "กระบวนการสำเร็จ",
          updatedAt: {
            $gte: dateStart,
            $lt: dateEnd
          }
        }
      },
      {
        $project: {
          day: {
            $substr: ["$updatedAt", 0, 10]
          },
          customerNumber: 1
        }
      },
      {
        $group: {
          _id: "$day",
          customerCount: {
            $sum: "$customerNumber"
          }
        }
      },
      {
        $sort: {
          _id: 1
        }
      }
      ])
        .then(customerCount => {
          result.customer = customerCount;
          // ยอดยกเลิก
          FoodMenuItem.aggregate([{
            $match: {
              status: "ยกเลิก",
              updatedAt: {
                $gte: dateStart,
                $lt: dateEnd
              }
            }
          },
          {
            $project: {
              day: {
                $substr: ["$updatedAt", 0, 10]
              },
              quantity: 1
            }
          },
          {
            $group: {
              _id: "$day",
              cancelCount: {
                $sum: "$quantity"
              }
            }
          },
          {
            $sort: {
              _id: 1
            }
          }
          ])
            .then(cancel => {
              result.cancel = cancel;
              res.status(200).json({
                success: true,
                data: result,
                message: "เรียกข้อมูลสำเร็จ"
              });
            })
            .catch(err => {
              res.status(404).json(err);
            });
        })
        .catch(err => {
          res.status(404).json(err);
        });
    })
    .catch(err => {
      res.status(404).json(err);
    });
};

exports.reportBill = (req, res, next) => {
  let dateStart = new Date(req.query.dateStart);
  dateStart.setUTCHours(24, 0, 0, 0);
  let dateEnd = new Date(req.query.dateEnd);
  dateEnd.setUTCHours(24, 0, 0, 0);
  let query = {
    updatedAt: {
      $gte: dateStart,
      $lt: dateEnd
    }
  };

  Bill.find(query)
    .populate("order")
    .then(result => {
      res.status(200).json({
        success: true,
        data: result,
        message: "เรียกข้อมูลสำเร็จ"
      });
    })
    .catch(err => {
      res.status(404).json(err);
    });
};

exports.reportCancel = (req, res, next) => {
  let dateStart = new Date(req.query.dateStart);
  dateStart.setUTCHours(24, 0, 0, 0);
  let dateEnd = new Date(req.query.dateEnd);
  dateEnd.setUTCHours(24, 0, 0, 0);
  let date = new Date(req.query.dateStart);
  let query = {};
  if (req.query.type === "ทั้งหมด") {
    query = {
      status: "ยกเลิก",
      updatedAt: {
        $gte: dateStart,
        $lt: dateEnd
      }
    };
  } else {
    query = {
      status: "ยกเลิก",
      updatedAt: {
        $gte: dateStart,
        $lt: dateEnd
      },
      "cancelOrder.by": req.query.type
    };
  }

  FoodMenuItem.find(query)
    .then(result => {
      res.status(200).json({
        success: true,
        data: result,
        message: "เรียกข้อมูลสำเร็จ"
      });
    })
    .catch(err => {
      res.status(404).json(err);
    });
};

exports.reportThisMonth = (req, res, next) => {
  const date = new Date(req.query.month)
  var ds = moment(date).startOf('month').format('YYYY-MM-DD');
  var de = moment(date).endOf('month').format('YYYY-MM-DD');

  var lastDateStart = moment(date).subtract(1, 'month').startOf('month').format('YYYY-MM-DD');
  var lastDateEnd = moment(date).subtract(1, 'month').endOf('month').format('YYYY-MM-DD');


  function getDates(startDate, stopDate) {
    var dateArray = []
    var currentDate = moment(startDate)
    var stopDate = moment(stopDate)
    while (currentDate <= stopDate) {
      dateArray.push(moment(currentDate).format('YYYY-MM-DD'))
      currentDate = moment(currentDate).add(1, 'days')
    }
    return dateArray
  }
  const dummyArray = getDates(ds, de)
  const dummyArrayLast = getDates(lastDateStart, lastDateEnd)
  let data = {}
  Bill.aggregate([{
    $project: {
      day: {
        $substr: ["$updatedAt", 0, 10]
      },
      totalPrice: 1
    }
  },
  {
    $group: {
      _id: "$day",
      income: {
        $sum: "$totalPrice"
      }
    }
  },
  {
    $sort: {
      _id: 1
    }
  },
  {
    $project: {
      date: '$_id',
      value: '$income',
    },
  },
  {
    $group: {
      _id: null,
      stats: {
        $push: "$$ROOT"
      }
    }
  },
  {
    $project: {
      stats: {
        $map: {
          input: dummyArray,
          as: "date",
          in: {
            $let: {
              vars: {
                dateIndex: {
                  "$indexOfArray": ["$stats._id", "$$date"]
                }
              },
              in: {
                $cond: {
                  if: {
                    $ne: ["$$dateIndex", -1]
                  },
                  then: {
                    $arrayElemAt: ["$stats", "$$dateIndex"]
                  },
                  else: {
                    _id: "$$date",
                    date: "$$date",
                    value: 0
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  {
    $unwind: "$stats"
  },
  {
    $replaceRoot: {
      newRoot: "$stats"
    }
  },
  {
    $group: {
      _id: "0",
      value: {
        $push: "$value"
      }
    }
  }
  ]).then((result) => {
    data.thisMonth = result
    Bill.aggregate([{
      $project: {
        day: {
          $substr: ["$updatedAt", 0, 10]
        },
        totalPrice: 1
      }
    },
    {
      $group: {
        _id: "$day",
        income: {
          $sum: "$totalPrice"
        }
      }
    },
    {
      $sort: {
        _id: 1
      }
    },
    {
      $project: {
        date: '$_id',
        value: '$income',
      },
    },
    {
      $group: {
        _id: null,
        stats: {
          $push: "$$ROOT"
        }
      }
    },
    {
      $project: {
        stats: {
          $map: {
            input: dummyArrayLast,
            as: "date",
            in: {
              $let: {
                vars: {
                  dateIndex: {
                    "$indexOfArray": ["$stats._id", "$$date"]
                  }
                },
                in: {
                  $cond: {
                    if: {
                      $ne: ["$$dateIndex", -1]
                    },
                    then: {
                      $arrayElemAt: ["$stats", "$$dateIndex"]
                    },
                    else: {
                      _id: "$$date",
                      date: "$$date",
                      value: 0
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    {
      $unwind: "$stats"
    },
    {
      $replaceRoot: {
        newRoot: "$stats"
      }
    },
    {
      $group: {
        _id: "0",
        value: {
          $push: "$value"
        }
      }
    }
    ]).then((result) => {
      data.lastMonth = result
      res.status(200).json({
        success: true,
        data: data
      })
    }).catch((err) => {
      res.status(400).json(err)
    });

  }).catch((err) => {
    res.status(400).json(err)
  });
}

exports.reportSummary = (req, res, next) => {
  const date = new Date(req.query.month)
  console.log(req.query.month);

  var dateStart = moment(date).startOf('month').format('YYYY-MM-DD');
  var dateEnd = moment(date).endOf('month').format('YYYY-MM-DD');

  var dateLastStart = moment(date).subtract(1, 'months').startOf('month').format('YYYY-MM-DD');
  var dateLastEnd = moment(date).subtract(1, 'months').endOf('month').format('YYYY-MM-DD');

  let resultReturn = {}

  Bill.aggregate([
    {
      $match: {
        updatedAt: {
          $gte: new Date(dateStart),
          $lt: new Date(dateEnd)
        }
      }
    },
    {
      $group: {
        _id: null,
        income: {
          $sum: "$totalPrice"
        }
      }
    }
  ]).then((result) => {
    resultReturn.thisMonth = result
    Bill.aggregate([
      {
        $match: {
          updatedAt: {
            $gte: new Date(dateLastStart),
            $lt: new Date(dateLastEnd)
          }
        }
      },
      {
        $group: {
          _id: null,
          income: {
            $sum: "$totalPrice"
          }
        }
      }
    ]).then((result) => {
      resultReturn.lastMonth = result
      res.status(200).json({
        success: true,
        data: resultReturn,
        message: "เรียกดูข้อมูลสำเร็จ"
      })
    }).catch((err) => {
      res.json(err)
    });

  }).catch((err) => {
    res.json(err)
  });
}

exports.reportYear = (req, res, next) => {
  const date = new Date(req.query.year)
  const year = moment(date).format('YYYY')

  let resultReturn = {}
  
  console.log(year);
  
  const dummyArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
  Bill.aggregate([
    {
      $project: {
        year: {
          "$year": "$updatedAt"
        },
        month: {
          "$month": "$updatedAt"
        },
        totalPrice: 1
      }
    },
    {
      $match: {
        year: year*1
      }
    },
    {
      $sort: {
        month: 1
      }
    },
    {
      $group: {
        _id: "$month",
        value: {
          $sum: "$totalPrice"
        }
      }
    },
    {
      $project: {
        date: "$_id",
        value: "$value"
      }
    },
    {
      $group: {
        _id: null,
        stats: {
          $push: "$$ROOT"
        }
      }
    },
    {
      $project: {
        stats: {
          $map: {
            input: dummyArray,
            as: "date",
            in: {
              $let: {
                vars: {
                  dateIndex: {
                    "$indexOfArray": ["$stats._id", "$$date"]
                  }
                },
                in: {
                  $cond: {
                    if: {
                      $ne: ["$$dateIndex", -1]
                    },
                    then: {
                      $arrayElemAt: ["$stats", "$$dateIndex"]
                    },
                    else: {
                      _id: "$$date",
                      date: "$$date",
                      value: 0
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    {
      $unwind: "$stats"
    },
    {
      $replaceRoot: {
        newRoot: "$stats"
      }
    },
    {
      $group: {
        _id: "0",
        value: {
          $push: "$value"
        }
      }
    }
  ]).then((result) => {
    resultReturn.thisYear = result
    Bill.aggregate([
      {
        $project: {
          year: {
            "$year": "$updatedAt"
          },
          month: {
            "$month": "$updatedAt"
          },
          totalPrice: 1
        }
      },
      {
        $match: {
          year: year-1
        }
      },
      {
        $sort: {
          month: 1
        }
      },
      {
        $group: {
          _id: "$month",
          value: {
            $sum: "$totalPrice"
          }
        }
      },
      {
        $project: {
          date: "$_id",
          value: "$value"
        }
      },
      {
        $group: {
          _id: null,
          stats: {
            $push: "$$ROOT"
          }
        }
      },
      {
        $project: {
          stats: {
            $map: {
              input: dummyArray,
              as: "date",
              in: {
                $let: {
                  vars: {
                    dateIndex: {
                      "$indexOfArray": ["$stats._id", "$$date"]
                    }
                  },
                  in: {
                    $cond: {
                      if: {
                        $ne: ["$$dateIndex", -1]
                      },
                      then: {
                        $arrayElemAt: ["$stats", "$$dateIndex"]
                      },
                      else: {
                        _id: "$$date",
                        date: "$$date",
                        value: 0
                      }
                    }
                  }
                }
              }
            }
          }
        }
      },
      {
        $unwind: "$stats"
      },
      {
        $replaceRoot: {
          newRoot: "$stats"
        }
      },
      {
        $group: {
          _id: "0",
          value: {
            $push: "$value"
          }
        }
      }
    ]).then((result) => {
      resultReturn.lastYear = result
      res.status(200).json({
        success: true,
        data: resultReturn,
        message: "เรียกดูข้อมูลสำเร็จ"
      })
    }).catch((err) => {
      res.json(err)
    });
  }).catch((err) => {
    res.json(err)
  });
}

exports.reportSummaryYear = (req, res, next) => {
  const date = new Date(req.query.year)
  console.log(date);

  var dateStart = moment(date).startOf('year').format('YYYY-MM-DD');
  var dateEnd = moment(date).endOf('year').format('YYYY-MM-DD');

  var dateLastStart = moment(date).subtract(1, 'year').startOf('year').format('YYYY-MM-DD');
  var dateLastEnd = moment(date).subtract(1, 'year').endOf('year').format('YYYY-MM-DD');

  let resultReturn = {}


  Bill.aggregate([
    {
      $match: {
        updatedAt: {
          $gte: new Date(dateStart),
          $lt: new Date(dateEnd)
        }
      }
    },
    {
      $group: {
        _id: null,
        income: {
          $sum: "$totalPrice"
        }
      }
    }
  ]).then((result) => {
    resultReturn.thisYear = result
    Bill.aggregate([
      {
        $match: {
          updatedAt: {
            $gte: new Date(dateLastStart),
            $lt: new Date(dateLastEnd)
          }
        }
      },
      {
        $group: {
          _id: null,
          income: {
            $sum: "$totalPrice"
          }
        }
      }
    ]).then((result) => {
      resultReturn.lastYear = result
      res.status(200).json({
        success: true,
        data: resultReturn,
        message: "เรียกดูข้อมูลสำเร็จ"
      })
    }).catch((err) => {
      res.json(err)
    });

  }).catch((err) => {
    res.json(err)
  });
}