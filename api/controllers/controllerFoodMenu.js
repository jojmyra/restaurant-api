const FoodMenu = require("../models/FoodMenu.model");

exports.getAll = (req, res, next) => {
  var options = JSON.parse(req.query.options);
  var query =
    options.search != ""
      ? {
          $or: [
            {
              foodName: { $regex: options.search }
            },
            {
              describe: { $regex: options.search }
            }
          ]
        }
      : {};
  if (options.searchType) query.foodType = options.searchType;
  FoodMenu.paginate(query, options)
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(400).json({
        message:
          "ไม่สามารถเรียกดูข้อมูลเมนูอาหารได้ในขณะนี้, กรุณาลองใหม่อีกครั้ง",
        success: false,
        err: err
      });
    });
};

exports.add = (req, res, next) => {  
  const menu = {
    foodName: req.body.foodName,
    foodType: req.body.foodType,
    price: req.body.price,
    describe: req.body.describe,
    calroies: req.body.calroies,
    foodImage: req.file.path
  }
  FoodMenu.create(menu)
    .then(result => {
      res.status(200).json({
        success: true,
        data: result,
        message: "เพิ่มข้อมูลสำเร็จ"
      });
    })
    .catch(err => {
      if (err.keyValue) {
        var keyValue = JSON.stringify(err.keyValue).split(":");
        var key = keyValue[0]
          .substring(2, keyValue[0].length - 1)
          .toUpperCase();
        var value = keyValue[1].substring(1, keyValue[1].length - 2);
      }
      var array = err.message.split(" ");
      var message = "";
      for (let index = 4; index < array.length; index++) {
        const element = array[index];
        message += `${element} `;
      }
      message = message.slice(0, -1);

      //validate error code
      if (err.code === 11000) {
        message = `${key} ${value} ถูกใช้งานเรียบร้อยแล้ว`;
      }
      res.status(200).json({
        success: false,
        message: message
      });
    });
};

exports.edit = (req, res, next) => {
  FoodMenu.findByIdAndUpdate(req.params.id, req.body.item)
    .then(result => {
      res.status(200).json({
        success: true,
        data: result,
        message: "เพิ่มข้อมูลสำเร็จ"
      });
    })
    .catch(err => {
      if (err.keyValue) {
        var keyValue = JSON.stringify(err.keyValue).split(":");
        var key = keyValue[0]
          .substring(2, keyValue[0].length - 1)
          .toUpperCase();
        var value = keyValue[1].substring(1, keyValue[1].length - 2);
      }
      var array = err.message.split(" ");
      var message = "";
      for (let index = 4; index < array.length; index++) {
        const element = array[index];
        message += `${element} `;
      }
      message = message.slice(0, -1);

      //validate error code
      if (err.code === 11000) {
        message = `${key} ${value} ถูกใช้งานเรียบร้อยแล้ว`;
      }
      res.status(200).json({
        success: false,
        message: message
      });
    });
};

exports.delete = (req, res, next) => {
  FoodMenu.findByIdAndRemove(req.params.id)
    .then(result => {
      res.status(200).json({
        success: true,
        message: "ลบข้อมูลสำเร็จ"
      });
    })
    .catch(err => {
      res.status(400).json({
        success: false,
        message: "ไม่สามารถลบข้อมูลได้, กรุณาลองใหม่อีกครั้ง"
      });
    });
};
