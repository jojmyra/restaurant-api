const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");

const config = require("config");
const dbConfig = config.get("database");

mongoose.connect(
  `mongodb+srv://${dbConfig.db_user}:${dbConfig.db_password}@${dbConfig.db_server}/${dbConfig.db_name}`,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    poolSize: 10 // Maintain up to 10 socket connections
  }
);
mongoose.Promise = global.Promise;

app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

const routesMain = require("./api/routes/mainRoutes");
app.use("/api", routesMain);
app.use(`/uploads`, express.static(`uploads`));

module.exports = app;
